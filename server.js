var express = require('express');
var app = express();  // Variable to get the Req and Res.
var cors = require('cors');
var path = require('path');
var bodyParser=require('body-parser'); 
var mongoose =require('mongoose');
var jwt = require('jwt-simple');
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var router = express.Router();

const port = 3000;
var User = require('./models/user.js');
var posts = [
    {message: 'Hi Parthiban'},
    {greetings: 'How are you'}
]

console.log('——————————- Run on port '+ port);



app.use(cors());

app.use(bodyParser.urlencoded({extended:true}));// get information from html forms
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride());
app.use('/', router); // app.use('/parent', router); call all from localhost:port/parent/*

// Point static path to public
app.use(express.static(path.join(__dirname, 'public')));

/****************************** Router ***************************/
router.get('*', function(req, res){
    res.sendFile('index.html', { root: __dirname + '/' });
});

/****************************** /Router ***************************/

app.get('/api/posts', (req,res) =>{
    res.send(posts);  
    
});


//Display Users from Backend

app.get('/api/users', async(req, res) =>{
    try{
        let users = await User.find({}, '-password -__v');
        res.send(users);
         }
         catch (error){
             console.log(error);
             res.sendStatus(200);
         }
})
app.get('/api/profile/:id', async(req,res)=> {
    try{
    let user= await User.findById(req.params.id,'-password -__v')
    res.send(user);
    }catch(error){
consolee.log(error);
res.sendStatus(500);
    }
})

app.post('/api/register', (req,res) =>{
       let userData = req.body;
        console.log(userData);

    let user = new User(userData);
    user.save((err,result) =>{
        if(err){
            console.log('There is trouble adding User Parthi.');
        }else{
            res.sendStatus(200);
        }
    })

});

//Token - End Point

app.post('/api/login', async(req,res) =>{
    let userData = req.body;
    let user = await User.findOne({email:userData.email});
    if (!user){
        return res.status(401).send({message: 'Email or Password invalid'})
    }
    if(userData.password !=user.password){
        return res.status(401).send({message: 'Password is invalid'})
    }
    let payload = {}
    let token = jwt.encode(payload, '23456')
    res.status(200).send({token});
});


mongoose.connect('mongodb://Nodeuser:Nodeuser@172.30.141.126:27017/IoT', { useNewUrlParser: true, useUnifiedTopology: true },(err) =>{
//mongoose.connect('mongodb://nodeuser:nodeuser@172.30.90.120:27017/iot', { useNewUrlParser: true, useUnifiedTopology: true },(err) =>{
//mongoose.connect('mongodb+srv://Priyankaa:ramapuram@priyankaa-kea1u.mongodb.net/test?retryWrites=true&w=majority',(err) =>{
if(!err){
   console.log('Connected to Database');
}    else
    console.log("Failed");
});


app.listen(port);