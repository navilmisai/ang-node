import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {
    constructor(public http: Http){}


    users = []

    getUsers() {
        this.http.get('http://olympians-iot-ocp.apps02-london.amosdemo.io/api/users/').subscribe(res =>{
        this.users = res.json()
        })

    }
    getProfile(id) {
        return this.http.get('http://olympians-iot-ocp.apps02-london.amosdemo.io/api/profile/' +id)
       
    }
   


    sendUserRegisteration(regData) {
        this.http.post('http://olympians-iot-ocp.apps02-london.amosdemo.io/api/register/', regData).subscribe(res =>{
        })

    }
   
    loginUser(loginData) {
        this.http.post('http://olympians-iot-ocp.apps02-london.amosdemo.io/api/login/', loginData).subscribe(res =>{
        console.log(res.ok);
        //console.log('thois ihgdhgdgfd' + res.ok);
        localStorage.setItem('token',res.json().token)
        })

    }
   

    }
